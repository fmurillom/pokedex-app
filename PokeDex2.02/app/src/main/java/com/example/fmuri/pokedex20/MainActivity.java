package com.example.fmuri.pokedex20;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    /**
     * Declaracion de las variables necesarioas
     */

    private String[] opciones = {"Pokedex", "Control"};
    private ListView mOptionListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /**
         * Control de la interfaz de la aplicacion
         */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mOptionListView = (ListView) findViewById(R.id.option_list_view);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, opciones);
        mOptionListView.setAdapter(adapter);

        /**
         * Que haccer al presionar alguna de las opciones
         */

        mOptionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.example.fmuri.pokedex");
                    if (launchIntent != null){
                        startActivity(launchIntent);
                    }
                }
                if (position == 1){
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.example.fmuri.control");
                    if (launchIntent != null){
                        startActivity(launchIntent);
                    }
                }
            }
        });
    }
}
