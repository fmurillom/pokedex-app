package com.example.fmuri.control;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.goebl.david.Webb;

public class MainActivity extends AppCompatActivity {
    /**
     * Declaracion de variables necesarias para la aplicacion
     */
    private static String TAG = MainActivity.class.getSimpleName();
    private Button UpButton;
    private Button DownButton;
    private Button LeftButton;
    private Button RightButton;
    private Button StartButton;
    private Button AButton;
    private Button SButton;
    private SendKeyTask mSendKeyTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /**
         * En esta funcion se ejecuta toda las funciones de interfaz de la aplicacion
         */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UpButton = (Button) findViewById(R.id.key_up_btn);
        DownButton = (Button) findViewById(R.id.key_down_btn);
        LeftButton = (Button) findViewById(R.id.key_left_btn);
        RightButton = (Button) findViewById(R.id.key_right_btn);
        StartButton = (Button) findViewById(R.id.key_enter_btn);
        AButton = (Button) findViewById(R.id.key_a_btn);
        SButton = (Button) findViewById(R.id.key_s_btn);

        /**
         * Declaraciones de las accionesa realizar dependiendo del boton presionado
         */

        UpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    mSendKeyTask = new SendKeyTask("up");
                    mSendKeyTask.execute((Void) null);
                } catch (Exception ex){
                    Log.e(TAG, ex.getLocalizedMessage(), ex);
                }
            }
        });

        DownButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                try{
                    mSendKeyTask = new SendKeyTask("down");
                    mSendKeyTask.execute((Void) null);
                } catch (Exception ex){
                    Log.e(TAG, ex.getLocalizedMessage(), ex);
                }
            }
        });

        LeftButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                try{
                    mSendKeyTask = new SendKeyTask("left");
                    mSendKeyTask.execute((Void) null);
                } catch (Exception ex){
                    Log.e(TAG, ex.getLocalizedMessage(), ex);
                }
            }
        });

        RightButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                try{
                    mSendKeyTask = new SendKeyTask("right");
                    mSendKeyTask.execute((Void) null);
                } catch (Exception ex){
                    Log.e(TAG, ex.getLocalizedMessage(), ex);
                }
            }
        });

        StartButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                try{
                    mSendKeyTask = new SendKeyTask("enter");
                    mSendKeyTask.execute((Void) null);
                } catch (Exception ex){
                    Log.e(TAG, ex.getLocalizedMessage(), ex);
                }
            }
        });

        AButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                try{
                    mSendKeyTask = new SendKeyTask("a");
                    mSendKeyTask.execute((Void) null);
                } catch (Exception ex){
                    Log.e(TAG, ex.getLocalizedMessage(), ex);
                }
            }
        });

        SButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                try{
                    mSendKeyTask = new SendKeyTask("s");
                    mSendKeyTask.execute((Void) null);
                } catch (Exception ex){
                    Log.e(TAG, ex.getLocalizedMessage(), ex);
                }
            }
        });
    }

    private class SendKeyTask extends AsyncTask<Void, Void, Boolean>{
        /**
         * Funcion que envia la tecla precionada
         */
        private String msendKey;

        SendKeyTask(String key){
            this.msendKey = key;
        }

        @Override
        protected Boolean doInBackground(Void... params){
            try{
                Webb webb = Webb.create();
                String response = webb.post("http://192.168.43.80:8080/api/v1/control")
                        .param("action", msendKey).asString().getBody();
                return response.equals("true");
            } catch (Exception ex){
                Log.e(TAG, ex.getLocalizedMessage(), ex);
                return false;
            }
        }
    }
}
