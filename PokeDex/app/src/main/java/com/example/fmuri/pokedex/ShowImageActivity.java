package com.example.fmuri.pokedex;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.goebl.david.Webb;

import static android.R.attr.bitmap;

public class ShowImageActivity extends AppCompatActivity {
    /**
     * Declaracion de variables necesarias para la aplicacion
     */
    private static String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog mProgressDialog;
    private String string_image;
    private ImageView mFotoview;
    private  RecieveBytesTask ReceiveImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /**
         * En esta funcion se ejecuta toda las funciones de interfaz de la aplicacion
         */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);
        String message = "Obteniendo Imagen del Servidor";
        mProgressDialog = ProgressDialog.show(ShowImageActivity.this, "", message, true);
        ReceiveImage = new RecieveBytesTask();
        ReceiveImage.execute((Void) null);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (string_image != null){
                    byte[] decoded_image = Base64.decode(string_image, 0);
                    final Bitmap bitmap = BitmapFactory.decodeByteArray(decoded_image, 0, decoded_image.length);
                    mFotoview = (ImageView) findViewById(R.id.pokemon_imgview);
                    mFotoview.setImageBitmap(bitmap);
                }
            }
        }, 3000);
    }

    private class RecieveBytesTask extends AsyncTask<Void, Void, Boolean>{
        /**
         * Funcion que recibe los bytes de la imagen almacenada en el servidor
         * @param params
         * @return true o false si la conexion es exitosa
         */

        @Override
        protected Boolean doInBackground(Void... params) {
            /**
             * Ejecuta en el fondo
             */
            try{
                Webb webb = Webb.create();
                String response = webb.get("http://192.168.43.80:8080/api/v1/pokemon/Dex_image")
                        .asString()
                        .getBody();
                string_image = response;
                return response.equals("true");
            } catch (Exception ex){
                Log.e(TAG, ex.getLocalizedMessage(), ex);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (mProgressDialog != null){
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        }
    }
}
