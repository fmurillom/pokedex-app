package com.example.fmuri.pokedex;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.goebl.david.Webb;

public class DownloadImageActivity extends AppCompatActivity {
    /**
     * Declaracion de variables necesarias para la aplicacion
     */
    private String nombres;
    private DownloadNameTask DownloadName;
    private SendIndexTask SendIndex;
    public String[] Dex;
    public static int index = 0;
    private ListView mDexEntry;
    private ProgressDialog mProgressDialog;

    private static final String TAG = DownloadNameTask.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /**
         * En esta funcion se ejecuta toda las funciones de interfaz de la aplicacion
         */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_image);
        mDexEntry = (ListView) findViewById(R.id.pokedex_view);
        String progressMsg = "Obteniendo datos del servidor";
        mProgressDialog = ProgressDialog.show(DownloadImageActivity.this, "", progressMsg, true);
        try {
            DownloadName = new DownloadNameTask();
            DownloadName.execute((Void) null);
        } catch (Exception ex){
            Log.e(TAG, ex.getLocalizedMessage(), ex);
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {
                if (Dex != null){
                    ArrayAdapter <String> adapter = new ArrayAdapter<String>(DownloadImageActivity.this, android.R.layout.simple_list_item_1, Dex);
                    mDexEntry.setAdapter(adapter);
                }
            }
        }, 1000);

        mDexEntry.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                index = position;
                SendIndex = new SendIndexTask();
                SendIndex.execute();
                startActivity(new Intent(DownloadImageActivity.this, ShowImageActivity.class));
            }
        });

    }

    public static int getIndex(){
        return index;
    }

    private class DownloadNameTask extends AsyncTask<Void, Void, Boolean>{
        /**
         * Funcion encargada de conesguir los nombres de los pokemones en el servidor
         * @param params
         * @return true o false dependiendo de si la conexion fue exitosa
         */
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Webb webb = Webb.create();
                String response = webb.get("http://192.168.43.80:8080/api/v1/pokemon/Dex").asString()
                        .getBody();
                Dex =  response.split(",");
                return true;
            } catch (Exception ex){
                Log.e(TAG, ex.getLocalizedMessage(), ex);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            /**
             * Una vez realizada la conexion muestra mensaje satisfactorio en caso de lograsrse la conexion
             */
            if (mProgressDialog != null){
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }

            if (success){
                Toast.makeText(getApplicationContext(), "Datos obtenidos del Servidor", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error de conexion", Toast.LENGTH_LONG).show();
            }
        }
    }
    private class SendIndexTask extends AsyncTask<Void, Void, Boolean>{
        /**
         * Funcion encargada de enviar el numero de indice del item seleccionado
         * @param params
         * @return true o false si la conexion es exitosa
         */

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                Webb webb = Webb.create();
                String response = webb.post("http://192.168.43.80:8080/api/v1/pokemon/Dex")
                        .param("indice", index)
                        .asString()
                        .getBody();
                return true;
            } catch (Exception ex){
                Log.e(TAG, ex.getLocalizedMessage(), ex);
                return false;
            }
        }
    }
}
